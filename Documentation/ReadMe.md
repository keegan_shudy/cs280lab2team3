---
output: pdf_document
---
#Running Instructions
1. First compile the program by using javac DataGenerator.java 
2. Run DataGenerator in the terminal with one command line argument that is a comma separated list of values
3. Example: 
	* java DataGenerator 1,2,3,4
 	* java DataGenerator a,b,c,d,e,f,g,h
 	
#Requirements
* java 8 or higher
