---
output: pdf_document
---
#Lessons Learned From Labs 2,3

###Keegan Shudy
For the past two labs I learned of the tricky and delicate system that is team collaberation.
Getting everyone together and making decisions for a large group on next steps is a difficult task
since everyone has their own opinions, ideas, and ways of doing things. As shown in the section
below, we overcame this difficulty eventually but at the time it was a set back.

###Brandon Ginoza
The main lessons that I learned while completing the last two labs was the challenges and advantages to working with a team. First of is organizing the team is a challenge that we overcame by using tools such as Slack and Bitbucket. And one of the advantages to working with a team was splitting up the work so that it is distributed evenly. Another lesson that I learned from these two labs was the importance of understanding someone else's code in order to write JUnit test cases to test their implementations. Also to go along with JUnit, I learned the importance of writing well documented Java source code that is split into appropriate methods. Therefore, it is easier to write JUnit test cases down the road during the testing phase of the software development process. 

###Jacob Hanko
The biggest lesson I learned from all of this was working and managing an entire team can be tricky.  Apart from distributing work evenly and communicating effectively (which are difficulties I already figured could occur), keeping a legible repository and agreeing on requirements could also be difficulties found in group projects.  I also learned a ton about testing, which gave me personally a new insight on what to have in code, so that it can easily be tested.  Another thing I learned was that documentation is a huge part of the project.  In a real world situation, a system without documentation is useless.

###Alex Means
As others have said, coordinating as a full team is difficult. Frequently, I felt the desire to work individually, only to realize that I should really consult the rest of my team first. I also did not quite realize how prevalent steps outside of implementation, such as design and documentation, were in real world projects. Ultimately, I think I learned a lot about how real-world projects work.







#Our Approach
For this lab we decided that we would first generally agree on how the system should be.
From here, changes to the program were made as necessary. We then decided to create two 
collaberating unit test classes. One for Team 3's implementation and another for Team 4's 
implementation. Then each person could individually add in their own tests that they wanted
to run without the hassle of creating four different files that all achieve the same end goal.
