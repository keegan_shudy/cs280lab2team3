<!-- Requirements Document --->

# DataGenerator

+ This program makes all possible lists from one list by swapping two objects at a time
	+ Ex: An input of {1, 2, 3} produces
		+ {1, 2, 3}
		+ {2, 1, 3}
		+ {3, 2, 1}
		+ {1, 3, 2}
	+ No lists need to be repeated, only listed once
	+ Output in a clean, readable output

+ This program can take in any input in the form of a list
	+ Ints, doubles, strings, chars, etc...

+ Easy to use program

+ Needs to be simplistic in nature in order to be implemented in a larger system 

+ Needs to be effectively implemented in one week's time


