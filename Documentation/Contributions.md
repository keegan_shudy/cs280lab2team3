---
output: pdf_document
---
#Contributors
+ Keegan Shudy
+ Jacob Hanko
+ Alex Means


#Roles
+ Keegan - finished the implementation of the system and the tutorial for how to use it, also wrote the JUnit test cases 
	for Team 4's implementation for Lab 3 and compiled a couple of the documents together.

+ Jacob - wrote up the requirements document
	+ Lab 3 -> Wrote the test cases that tested Team 3's (our own) system.  Wrote the tutorial on how to compile and run the tests.

+ Alex - wrote the design for the implementation based on the requirments
