#Contributors
+ Keegan Shudy
+ Jacob Hanko
+ Alex Means



#Roles
+ Keegan - finished the implementation of the system and the tutorial for how to use it

+ Jacob - wrote up the requirements document

+ Alex - evaluated both our team and our partner team
