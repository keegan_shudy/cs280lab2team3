/*
 * 
 * Take a comma separated list and print out all possible occurances of that 
 * list for swaped positions 
 *
 * @author Keegan Shudy
 */
 import java.util.Arrays;
public class DataGenerator {

    /**
     * @param args the command line arguments
     * These should be in the form of a comma separated value list
     */
    public static void main(String[] args) {
        String values = "";
        String [] parsed;
        String [] holder;
        try{
            values = args[0];
            if(args.length > 1){
            	System.out.println("No spaces are allowed, please separate values by a comma\n");
            	System.exit(-1);
            }
       }catch(Exception e){
            System.out.printf("%s", "Please pass a command line comma separated list when "
                    + "running this file\n"
                    + "Ex: java DataGenerator 1,2,3,4\n"
                    );
            System.exit(-1);
        }
        parsed = parse(values);
        holder = parsed.clone();
        
        System.out.println(Arrays.toString(holder)); //don't forget the first one ;)
        
        //swap positions 0,1 ; 0,2 ; 0,3 ;; 1,2 ....
        for(int i = 0; i < parsed.length-1; i++){
            for(int j = i+1; j < parsed.length; j++){
		    swap(holder, i, j);
		    System.out.println(Arrays.toString(holder));
		    holder = parsed.clone();
            }
        }
    }
    
    public static String[] parse(String list){
        if(list.length() == 1)
            return(new String[]{list});
        
        String[] parsed = list.split(",");
        
        
        return parsed;
    }
    
    public static String[] swap(String [] list, int startPos, int endPos){
        String temp = list[startPos];
        list[startPos] = list[endPos];
        list[endPos] = temp;
        
        return list;
    }
    
}
