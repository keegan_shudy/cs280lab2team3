Laboratory 3 - Team 3

Jacob Hanko

Keegan Shudy

Alex Means

# DataGeneratorTests Tutorial

## Step 1: Make Sure Our Repository is Local

If you have not already cloned our repository into a local folder, please do so.

Visit bitbucket.org/keegan_shudy/cs280lab2team3 and clone from there.

## Step 2: Locate the DataGenerator Folder

cs280f2015lab2team3 -> Implementation -> DataGenerator

## Step 3: Change Your Classpath

You need to make sure your classpath is set to include both the junit-4.xx.jar file as well as the hamcrest-core-1.3.jar file.  Open up a Terminal window, and it should look similar to the example shown below.

```shell
export CLASSPATH="/home/h/hankoj/cs280f2015/
cs280lab2team3/Implementation/DataGenerator/
junit-4.12.jar/:/home/h/hankoj/cs280f2015/
cs280lab2team3/Implmentation/DataGenerator/
hamcrest-core-1.3.jar:."
```

## Step 4: Compiling and Running the Tests

The first thing you should do is make sure all of the Java files are compiled.  Type the code below in a Terminal window that is pointing towards the correct location in the directory where the DataGenerator folder is stored.

```shell
javac *.java
```

From there, run the tests by typing the following code into the Terminal:

```shell
java -cp junit-4.12.jar:hamcrest-core-1.3.jar:.
org.junit.runner.JUnitCore DataGeneratorTests

```

You should see output in your Terminal window.

```shell
JUnit version 4.xx
.....
Time: x.xxx

OK (x tests)
```
