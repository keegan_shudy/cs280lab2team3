// Lab 3 
// Tests for DataGenerator
// Team 3
// Jacob Hanko
//

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import java.util.*;
import java.io.*;

// Below are the JUnit tests for our groups' program, DataGenerator


public class DataGeneratorTests {

	// This test makes sure that the swapping method functions properly
	// Swapping the first and second spots of the array accordingly
	@Test
	public void testSwappingSpot1And2() {
		String[] input = {"1", "2", "3", "4"};
		String[] expected = {"2", "1", "3", "4"};

		assertArrayEquals(expected, DataGenerator.swap(input,0,1));
	}

	// This test makes sure that other input (chars) other than numbers can be inputted by the program
	// (Yes I know that it takes in string values by default)
	@Test
	public void testToSeeIfCharsSwap() {
		String[] input = {"a", "b", "c", "d"};
		String[] expected = {"b", "a", "c", "d"};

		assertArrayEquals(expected, DataGenerator.swap(input,0,1));
	}

	// This test is similar to the one above, just with strings
	// (Same parenthetical side note as above)
	@Test
	public void testToSeeIfWordsSwap() {
		String[] input = {"aaa", "bbb", "ccc", "ddd"};
		String[] expected = {"bbb", "aaa", "ccc", "ddd"};

		assertArrayEquals(expected, DataGenerator.swap(input,0,1));
	}

	// This test makes sure that the parse method correctly takes in string values
	// and puts them properly into a String array, with commas separating the values
	@Test
	public void testToMakeSureParseMethodCreatesArray() {
		String input = "1,2,3,4";
		String[] expected = {"1", "2", "3", "4"};

		assertArrayEquals(expected, DataGenerator.parse(input));

	}

	// This test makes sure that both the parse and swap works when called
	// together
	@Test
	public void testToTryAParseAndASwap() {
		String input = "1,a,2,b";
		String[] expectedFinalOutput = {"1","b","2","a"};

		assertArrayEquals(expectedFinalOutput, (DataGenerator.swap(DataGenerator.parse(input),1,3)));
	}

}
